""" Modulo para la clase Cache """

from robot import Robot

class Cache:

    # Argumentos de la Clase Cache
    def __init__(self):
        self.cache = {}

    # Metodo retrieve en el que se descarga el documento de la url
    # si no ha sido antes descargado
    def retrieve(self, url):
        if url not in self.cache:
            self.cache[url] = Robot(url)

    # Método content que almacena el contenido de la url de la cache
    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    # Metodo show que muestra el contenido de la url de la caché
    def show(self, url):
        print(self.content(url))

    # Metodo show_all que muestra el contenido de todas las urls de la cache
    def show_all(self):
        for url in self.cache:
            print(url)

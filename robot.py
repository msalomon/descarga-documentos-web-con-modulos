""" Modulo para la clase Robot """

import urllib.request
class Robot:

    # Argumentos de la Clase Robot
    def __init__(self, url):
        self.url = url          # Almacena la url proporcionada
        self.retrieved = None   # Atributo para almacenar el contenido descargado
                                # Lo inicializo a None

    # Metodo retrieve en el que se descarga el documento de la url
    def retrieve(self):
        if self.retrieved is None:
            print(f"Descargando {self.url}")
            response = urllib.request.urlopen(self.url)
            self.content = response.read().decode('utf-8')
            self.retrieved = 1

    # Método content que almacena el contenido que se descarga de la url
    def content(self):
        self.retrieve()
        return self.content

    # Metodo show que muestra el contenido de la url descargada
    def show(self):
        print(self.content())



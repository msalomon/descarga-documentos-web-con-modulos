""" Programa principal para la solucion del ejercicio
    "Descarga de documentos web" con modulos """


from robot import Robot
from cache import Cache

if __name__ == '__main__':
    print("--------------- ROBOT 1 ---------------")
    r1 = Robot('https://www.aulavirtual.urjc.es/')
    r1.show()

    print("--------------- ROBOT 2 ---------------")
    r2 = Robot('https://www.urjc.es/')
    r2.retrieve()

    print("--------------- ROBOT 3 ---------------")
    r2.retrieve()  # No deberia imprimir nada porque la url ya se la ha descargado el robot 2 antes

    c = Cache()
    print("--------------- CACHÉ 1 ---------------")
    c.retrieve('https://gitlab.eif.urjc.es/')

    print("--------------- CACHÉ 2 ---------------")
    c.show('http://gsyc.urjc.es/')

    print("--------------- CACHÉ 3 ---------------")
    c.show_all()
